import { Body, Controller, Get, Post } from "@nestjs/common";
import { AppService } from './app.service';
import { Observable } from "rxjs";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/create')
  create(@Body() data): Observable<any> {
    return this.appService.create(data);
  }
  @Get('/list')
  travelopiaList(): Observable<any> {
    return this.appService.travelopiaList();
  }

}
