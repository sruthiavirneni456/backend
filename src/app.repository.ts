import { Model } from "mongoose";
import { Injectable, Logger } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { TravelopiaFormData, TravelopiaFormDocument } from "./app.schema";
import { from, Observable } from "rxjs";

@Injectable()
export class TravelopiaRepository {
  constructor(
    @InjectModel('Travelopia')
    private TravelopiaForm: Model<TravelopiaFormDocument>,
  ) {
  }


  create(travelopiaFormDocument: TravelopiaFormData): Observable<any> {
    Logger.log('Inside TravelopiaRepository::::::::');

    return from(this.TravelopiaForm.create(travelopiaFormDocument));
  }

  travelopiaList(): Observable<any> {
    return from(this.TravelopiaForm.find());
  }

}
