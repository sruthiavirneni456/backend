import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from "@nestjs/mongoose";
import { TravelopiaFormSchema } from "./app.schema";
import { TravelopiaRepository } from "./app.repository";

@Module({
  imports: [
    MongooseModule.forRoot(
      'use your Mongodb url here',
    ),
    MongooseModule.forFeature([
      {
        name: 'Travelopia',
        schema: TravelopiaFormSchema,
        collection: 'Travelopia',
      },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService, TravelopiaRepository],
})
export class AppModule {}
