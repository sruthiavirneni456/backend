import { Injectable } from '@nestjs/common';

import { Observable } from "rxjs";
import { TravelopiaFormData } from "./app.schema";
import { TravelopiaRepository } from "./app.repository";

@Injectable()
export class AppService {
  constructor(private readonly travelopiaRepository: TravelopiaRepository) {}
  create(data): Observable<any> {
    const travelopiaDocument: TravelopiaFormData = new TravelopiaFormData();

    travelopiaDocument._id = data?._id;
    travelopiaDocument.name = data?.name;
    travelopiaDocument.emailAddress = data?.emailAddress;
    travelopiaDocument.location = data?.location;
    travelopiaDocument.budget = data?.budget;
    travelopiaDocument.travellers = data?.travellers;

    return this.travelopiaRepository?.create(travelopiaDocument);
  }

  travelopiaList(): Observable<any> {
    return this.travelopiaRepository?.travelopiaList();
  }
}
