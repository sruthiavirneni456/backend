import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';


export type TravelopiaFormDocument = TravelopiaFormData & Document;

@Schema()
export class TravelopiaFormData {
  _id: string;

  @Prop({ type: String })
  name: string;

  @Prop({ type: String })
  emailAddress: string;

  @Prop({ type: String })
  location: string;

  @Prop({ type: Number })
  travellers: number;

  @Prop({ type: Number })
  budget: number;

}

export const TravelopiaFormSchema =
  SchemaFactory.createForClass(TravelopiaFormData);
